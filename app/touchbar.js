// Require the 'TouchBar' module from electron
const { app, TouchBar, BrowserWindow } = require("electron");

// Require all the types of touchbar inputs (group, picker, slide, button, label etc..)
const {
  TouchBarButton,
  TouchBarGroup,
  TouchBarSpacer,
  TouchBarLabel
} = TouchBar;

// Shorthand for spacer
const spacer = (size = "flexible") => {
  return new TouchBarSpacer({ size });
};

// Generate a new touchbar object
const touchbar = new TouchBar([
  new TouchBarGroup({
    items: [
      new TouchBarButton({
        label: "John Cena",
        // You can also define an action with click() method
        click() {
          BrowserWindow.getFocusedWindow().webContents.send("play");
        }
      }),
    ]
  }),
  spacer(),
]);

module.exports = touchbar;
