# Electron _Touch Bar_ API
> Electron example  [Touch Bar API](https://electron.atom.io/docs/api/touch-bar/)

Forked from [https://github.com/Rawnly/touchbar-api-sample](https://github.com/Rawnly/touchbar-api-sample)
![screenshot](screenshots/window.png)

# Installation
1. Clone this repo and extract it
2. Install dependencies (`yarn`)
3. Run it via `yarn start`

# Usage
Once the app is running, play with the `Touch Bar`! It you should be something like this one below:

There are all `TouchBar` elements:
- `Button` - Restore Defaults (restore defaults parameters)
- `Color Picker` - (change text color)
- `Slider` - Inside options (change the font size of the `H1`)
- `Group` - `Button`, `PopOver`, `Color Picker`
- `Scrubbler` - Inside options  (change the `H1` text)
- `Pop Over` - Options
- `Segmented Controller` - The On\Off switch (toggle if color can change true/false)
- `Spacer`
- `Label` - App name
---

# Code of conduct
In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and[...]

- [Code Of Conduct](code-of-conduct.md)

# LICENSE (MIT)
See *LICENSE.md*
---

